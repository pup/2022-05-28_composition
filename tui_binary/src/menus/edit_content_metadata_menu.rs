pub struct EditContentMetadataMenu
{
    pub menu:tui_utilities::utilities::menu::Menu,
    pub delete_notice:bool,
    pub cancel_notice:bool,
    pub edit_metadata_notice:bool,
    pub edit_contents_notice:bool,
}
impl EditContentMetadataMenu
{
    pub fn new_edit_content_metadata_menu
    (
      owner:usize,
      position:tui_utilities::utilities::position::Position,
      world:std::rc::Rc<std::cell::RefCell<bevy_ecs::world::World>>,
    )
    {
        /*
          5
        114            Edit....
        2 3
        3 2 cancel delete metadata content
          1234567891234567891234567891234567
           1234567891234
           12
           123456789
           1234567891234567
           1234567891234567891234567
        */
        let mut borrowed_world = world.borrow_mut();
        let mut menus = borrowed_world.get_resource_mut::<Vec<std::sync::Arc<std::sync::Mutex<dyn tui_utilities::utilities_operations::menu_operations::MenuOperations>>>>().unwrap();
        let menu_position = menus.len();
        let mut component_list:Vec<tui_utilities::utilities::component::Component> = Vec::new();
        
    }
}